### 谈谈你是如何理解JS异步编程的EventLoop、消息队列都是做什么的，什么是宏任务，什么是微任务

JS是单线程的语言，也就是说同一个时间只能处理一个任务。而为了协调事件、用户交互、脚本、UI渲染和网络处理等行为，防止主线程的不阻塞，（事件循环）EventLoop 的方案应用而生。


JS处理任务是在等待任务、执行任务、休眠等待新任务中不断循环中，也称这种机制为事件循环
- 主线程中的任务执行完后，才执行任务队列中的任务
- 有新任务到来时会将其放入队列，采取先进先执行的策略执行队列中的任务
- 比如多个setTimeout同时到时间了，需要依次执行

任务包括script(整体代码)、setTimeout、setInterval、DOM渲染、DOM事件、Promise、XMLHttpRequest等等

宏任务：当前调用栈中执行的代码成为宏任务
包括：I/O、setTimeout、setInterval、requestAnimationFrame、setImmediate、js代码块

微任务：当前（此次事件循环中）宏任务执行完，在下一个宏任务开始之前需要执行的任务，可以理解为回调事件。（process.nextTick、MutationObserver、Promise.then catch finally）

宏任务中的事件放在callback queue中，由事件触发线程维护；微任务的事件放在微任务队列中，由js引擎线程维护。