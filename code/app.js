const fp = require('lodash/fp');
/**
 * 一、将下面异步代码使用Promise的方式改进
 */
setTimeout(function() {
  var a = 'hello'
  setTimeout(function() {
    var b = 'lagou'
    setTimeout(function() {
      var c = 'I ❤️ U'
      console.log(a + b + c)
    }, 10)
  }, 10)
}, 10)

var p1 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('hello')
  }, 10)
})
var p2 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('lagou')
  }, 10)
})
var p3 = new Promise((resolve, reject) => {
  setTimeout(() => {
    resolve('I ❤️ U')
  }, 10)
})
Promise.all([p1, p2, p3])
.then(val => {
  console.log(val.join(''))
})

/*
 * 二、基于以下代码完成下面的四个练习
 */
const cars = [
  {
    "name": "Ferrari FF",
    "horsepower": 660,
    "dollar_value": 700000,
    "in_stock": true
  },
  {
    "name": "Spyker C12 Zagato",
    "horsepower": 650,
    "dollar_value": 648000,
    "in_stock": false
  },
  {
    "name": "Jaguar XKR-S",
    "horsepower": 550,
    "dollar_value": 132000,
    "in_stock": false
  },
  {
    "name": "Audi R8",
    "horsepower": 525,
    "dollar_value": 114200,
    "in_stock": false
  },
  {
    "name": "Aston Martin One-77",
    "horsepower": 750,
    "dollar_value": 1850000,
    "in_stock": true
  },
  {
    "name": "Pagani Huayra",
    "horsepower": 700,
    "dollar_value": 1300000,
    "in_stock": false
  }
]


// 练习1：使用函数组合 fp.flowRigh()重新实现下面这个函数
// let isLastInStock = function (cars) {
//   let last_car = fp.last(cars)
//   return fp.prop('in_stock', last_car)
// }
let isLastInStock = fp.flowRight(fp.prop('in_stock'), fp.last);
console.log(isLastInStock(cars))


// 练习2：使用 fp.flowRight()、fp.prop() 和 fp.first() 获取第一个car的name
let fistName = fp.flowRight(fp.prop('name'), fp.first);
console.log(fistName(cars))


// 练习3：使用帮助函数_average重构
// averageDollarValue，使用函数组合的方式实现
let _average = function(xs) {
  return fp.reduce(fp.add, 0, xs) / xs.length
}
let averageDollarValue = function(cars) {
  let value = fp.map(function(car) {
    return car.dollar_value
  }, cars)
  return _average(value)
}
let resultValue = fp.flowRight(_average, fp.map(val => val.dollar_value))
console.log(resultValue(cars))


// 练习4：使用 flowRight 写一个 sanitizeNames() 函数，
// 返回一个下划线连接的小写字符串，把数组中的 name 转换为这种形式：
// 例如：sanitizeNames([“Hello World”]) => [“hello_world”]
let _underscore = fp.replace(/\W+/g, '_');
let sanitizeNames = fp.flowRight(fp.map(fp.flowRight(fp.toLower, _underscore)));
console.log(sanitizeNames(['Hello World']))


/*
 * 三、基于下面提供的代码，完成后续的四个练习
 */

const { Maybe, Container } = require('./support.js')
let maybe = Maybe.of([5, 6, 1])


// 练习1：使用 fp.add(x, y) 和 fp.map(f, x) 创建一个能让 functor 里的值增加的函数 ex1
let ex1 = x => maybe.map(list => fp.map(v => fp.add(v, x), list))
console.log(ex1(2))


// 练习2：实现一个函数 ex2，能够使用 fp.first 获取列表的第一个元素
let xs = Container.of(['do', 'ray', 'me', 'fa', 'so', 'la', 'ti', 'do'])
let ex2 = () => xs.map(list => fp.first(list))
console.log(ex2())


// 练习3：实现一个函数 ex3 ，使用 safeProp 和 fp.first 找到 user 的名字的首字母
let safeProp = fp.curry(function(x, o) {
  return Maybe.of(o[x])
})
let user = { id: 2, name: 'Albert' }
let ex3 = () => safeProp('name', user).map(x => fp.first(x))
console.log(ex3())

// 练习4：使用 Maybe 重写 ex4 ，不要有 if 语句
let ex4 = n => Maybe.of(n).map(x => parseInt(x))
console.log(ex4('2124'))